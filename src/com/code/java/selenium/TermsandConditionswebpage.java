package com.code.java.selenium;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;



public class TermsandConditionswebpage {
	private static WebDriver driver=null;
	private static DesiredCapabilities cap;

	private static WebDriverWait wait;
	@BeforeClass
	public static void setUp() throws MalformedURLException {
		cap = DesiredCapabilities.chrome();
		// set up appium
		File appDir = new File("Downloads");
		File app = new File(appDir,"buzz.apk");
		DesiredCapabilities capabilities;
		capabilities = new DesiredCapabilities();
//		capabilities.setCapability("MobileCapabilityType.PLATFORM_NAME","MobilePlatform.ANDROID");
		capabilities.setCapability("device", "Android");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "LGE Nexus 5");
		capabilities.setCapability("app", app.getAbsolutePath()); 
		driver = new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"),capabilities);
		wait = new WebDriverWait(driver,30);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	}
	
	@Test
	public void TermsandConditionswebpageTest() throws InterruptedException
	{
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/skip_button")).click();
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/login_button")).click();
		
		List<WebElement> a = driver.findElements(By.className("android.widget.EditText"));
		a.get(0).sendKeys("avishwekar@buzzpoints.com");
		a.get(1).sendKeys("aaaaaa");
		driver.navigate().back();
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/email_sign_in_button")).click();
		Thread.sleep(6000);
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/manage_account_button")).click();
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/view_terms_and_conditions")).click();
		System.out.println("Test Passed :++++++++++"+this.getClass()+"++++++++++");
	}
}
