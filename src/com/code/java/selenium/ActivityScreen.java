package com.code.java.selenium;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.code.java.api.*;


public class ActivityScreen extends StartWebDriver {

	
	@Test
	public void ActivityScreenTest() throws InterruptedException
	{
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/skip_button")).click();
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/login_button")).click();
		
		List<WebElement> a = driver.findElements(By.className("android.widget.EditText"));
		a.get(0).sendKeys("avishwekar@buzzpoints.com");
		a.get(1).sendKeys("aaaaaa");
		driver.navigate().back();
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/email_sign_in_button")).click();
		Thread.sleep(6000);
		driver.findElement(By.id("com.fisoc.buzzpoints.dev:id/activity_button")).click();
		System.out.println("Test Passed :++++++++++"+this.getClass()+"++++++++++");
	}
}
