package com.code.java.api;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;



public class StartWebDriver {
	protected static WebDriver driver=null;
	private static DesiredCapabilities cap;

	private static WebDriverWait wait;
	@BeforeClass
	public static void setUp() throws MalformedURLException {
		cap = DesiredCapabilities.chrome();
		// set up appium
		File appDir = new File("Downloads");
		File app = new File(appDir,"buzz.apk");
		DesiredCapabilities capabilities;
		capabilities = new DesiredCapabilities();
//		capabilities.setCapability("MobileCapabilityType.PLATFORM_NAME","MobilePlatform.ANDROID");
		capabilities.setCapability("device", "Android");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "LGE Nexus 5");
		capabilities.setCapability("app", app.getAbsolutePath()); 
		driver = new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"),capabilities);
		wait = new WebDriverWait(driver,30);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	}
	

	@AfterClass(alwaysRun=true)
	public void tearDown() {
		try {
		    System.out.printf("%n[END] Thread Id : %s", Thread.currentThread().getId());
			driver.close();
			driver.quit();
			if(driver != null)
				driver = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
